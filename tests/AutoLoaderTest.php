<?php

use PHPUnit\Framework\TestCase;

class AutoLoaderTest extends TestCase {

	/**
	 * @test classExits
	 */
	public function testClassExists() {
		require dirname(__FILE__).'/../otama-core.php';
		$this->assertTrue(class_exists('OT_Controller'));
		$this->assertTrue(class_exists('OT_BaseModel'));
		$this->assertTrue(class_exists('OT_BaseModelProperty'));
		$this->assertTrue(class_exists('OT_BaseRecord'));
		$this->assertTrue(class_exists('OT_BaseRecordCollection'));
		$this->assertTrue(class_exists('OT_Common'));
		$this->assertTrue(class_exists('OT_Controller'));
		$this->assertTrue(class_exists('OT_Params'));
		$this->assertTrue(class_exists('OT_RecordErrorCollection'));
		$this->assertTrue(class_exists('OT_Render'));
		$this->assertTrue(class_exists('OT_Router'));
		$this->assertTrue(class_exists('OT_Validator'));
		$this->assertTrue(class_exists('OT_ValidationError'));
		$this->assertTrue(class_exists('BaseRecordValidationError'));
	}

}