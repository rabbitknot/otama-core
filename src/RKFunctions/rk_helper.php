<?php

if (! function_exists('is_set_def')){
	function is_set_def($value, $default) {
		return isset($value) ? $value : $default;
	}
}

if (! function_exists('is_not_emp_def')){
	function is_not_emp_def($value, $default) {
		return empty($value) ? $default : $value;
	}
}

/**
 * echo alias
 */
if (! function_exists('e')){
	function e($text, $out=true, $alt=null) {
		echo $out ? $text : $alt;
	}
}

if (! function_exists('etext')) {
	function etext($text)
	{
		echo nl2br($text);
	}
}

/**
 * htmlspecialchars alias
 */
if (! function_exists('h')) {
	function h($text)
	{
		if (is_object($text)) {
			$text = strval($text);
		}
		return htmlspecialchars($text, ENT_QUOTES);
	}
}

/**
 * echo + htmlspecialchars alias
 */
if (! function_exists('eh')) {
	function eh($text, $out = true, $alt = null)
	{
		echo $out ? h($text) : $alt;
	}
}

if (! function_exists('ehtext')) {
	function ehtext($text)
	{
		echo nl2br(h($text));
	}
}


/**
 * echo + rawurlencode のエイリアス
 * @return void
 */

if (! function_exists('eu')) {
	function eu($text, $alt = null)
	{
		if ((!is_null($text) and $text !== '')
			or 1 === func_num_args()) {
			echo rawurlencode($text);
		} else {
			echo rawurlencode($alt);
		}
	}
}