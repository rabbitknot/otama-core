<?php

if (!class_exists('OT_Common', false)) {
	class OT_Common {
		public static function show_404($message = "" ) {
			OT_Render::getInstance()->display('error',array(
				'err_title' => 'リクエストエラー',
				'type' => '404',
				'message' => isset($message) ? $message : '無効なリクエストです。'
			));
			exit;
		}

		public static function get_the_user_ip() {
			if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
				//check ip from share internet
				$ip = $_SERVER['HTTP_CLIENT_IP'];
			} elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
				//to check ip is pass from proxy
				$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			} else {
				$ip = $_SERVER['REMOTE_ADDR'];
			}
			return $ip;
		}
	}
}

