<?php

if (!class_exists('OT_BaseModel', false)) {
	require_once 'OT_BaseModel.php';

	class OT_BaseRecord {

		/**
		 * このレコードに紐づくモデル
		 */
		private $_model;

		/**
		 * 新しいレコードか否か
		 */
		private $_is_new;

		/**
		 * エラー情報の格納
		 */
		private $_errors;

		public final function __construct(OT_BaseModel $model, $attrs=array()) {
			$this->_model = $model;
			$this->_is_new = true;
			$this->_errors = new OT_RecordErrorCollection();
			$this->set($attrs);
		}


		public function model() {
			return $this->_model;
		}

		/**
		 * まだデータベースに保存されていないインスタンスかどうか確認または設定する。
		 * @param boolean $state[optional]
		 * @return boolean
		 */
		public function is_new($state=null) {
			if (!is_null($state)) {
				$this->_is_new = (bool) $state;
			}
			return $this->_is_new;
		}

		/**
		 * レコードに属性をセットする
		 * @param mixed $one 属性値を含む連想配列、または属性名
		 * @param mixed[optional] $one が属性名の時のその値
		 * @return void
		 */
		public function set($one, $two=null) {
			if (is_array($one) or is_object($one)) {
				foreach ($one as $key => $value) {
					$this->$key = $value;
				}
			} else {
				$this->$one = $two;
			}
		}

		public function get($name, $default=null) {
			if (isset($this->$name)) {
				return $this->$name;
			} else {
				return $default;
			}
		}


		/**
		 * バリデーションを実行する。
		 * 通常、問題があった時には、DNV_RecordValidationError をスローする。
		 * $throws が false の時には、バリデーションエラー時には false を返す。
		 * その他は true を返す。
		 * @param bool $throws
		 * @throws bool|BaseRecordValidationError
		 */
		public function validate($throws=true) {

			$this->before_validate();
			$this->model()->validate_record_by_definition($this);
			$this->after_validate();

			if ($this->haserr()) {
				if ($throws) {
					throw new BaseRecordValidationError($this->geterr()."\n確認して再度、登録してください。");
				} else {
					return false;
				}
			} else {
				return true;
			}
		}

		/**
		 * 指定の属性を、与えられたルールでバリデーションする
		 * @param string $attr 属性名
		 * @param string $rules validator::apply の仕様によるバリデーションルール。
		 */
		public function validate_for($attr, $rules) {

			$self = $this;
			$this->$attr = OT_Validator::apply(
				$this->$attr, $rules, $self, $attr
			);
			return $this->haserr($attr);
		}

		protected function before_validate() {

		}

		protected function after_validate() {

		}

		protected function before_save() {

		}

		protected function after_save() {

		}

		public function save($validate=true) {
			if ($validate) {
				$this->validate();
			}

			$this->before_save();

			if ($this->is_new()) {
				$data = $this->get_persistent_data();
				$this->id = $this->model()->insert($data);
			} else {
				$data = $this->get_persistent_data();
				$this->model()->update($data, $this->id);
			}
		}

		/**
		 * 永続化対象のデータを配列で取得する。
		 * @return array
		 */
		public function get_persistent_data() {
			$data = get_object_vars($this);
			return $this->model()->extract_persistent_data($data);
		}







		/**
		 * エラーの有無を調べる
		 * @return $name 指定すればその名前のエラーの有無を、未指定ならエラーを１つでも含むか？
		 * @return bool
		 */
		public function haserr($name=null) {
			if (empty($name)) {
				return ! $this->_errors->is_empty();
			} else {
				return $this->_errors->has($name);
			}
		}

		/**
		 * エラーメッセージを取得する
		 * @return $name
		 * @return string
		 */
		public function geterr($name=null, $glue='') {
			if ($name) {
				return $this->_errors->get($name);
			} else {
				return strval($this->_errors);
			}
		}

		/**
		 * エラーメッセージを設定する
		 * @return $name
		 * @return string
		 */
		public function adderr($name, $message) {
			$this->_errors->add($name, $message);
		}

		/**
		 * 属性値の取得
		 * @param string $name
		 * @return mixed
		 */
		public function __get($name) {
			if ($name === 'errors') {
				return $this->_errors;
			} else {
				return $this->get($name);
			}
		}


		/**
		 * '%%%%%'区切りの文字列に新たに文字列を追加・更新する。
		 * 重複が起こらないようチェックして、新たな要素を追加する。
		 * company_name_for_search, company_en_name, compnay_short_en_nameなどに使用
		 * @param $before_string
		 * @param $append_string
		 * @return string
		 */
		public function update_string_for_search($before_string, $append_string) {
			$updated_string = explode('%%%%%', "$before_string");
			$updated_string = array_unique($updated_string);
			$appends = explode('%%%%%', "$append_string");
			foreach($appends as $word) {
				if (!in_array($word, $updated_string)) {
					$updated_string[] = $word;
				}
			}
			$removed_empty = array();
			foreach($updated_string as $str) {
				if (!empty($str)) {
					$removed_empty[] = $str;
				}
			}
			return implode('%%%%%', $removed_empty);
		}
	}
}