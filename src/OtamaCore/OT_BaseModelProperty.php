<?php

if (!class_exists('OT_BaseModelProperty', false)) {
	class OT_BaseModelProperty
	{
		public static function create($label, $defaultValue, $validation=null) {
			return new OT_BaseModelProperty($label, $defaultValue, $validation);
		}
		public $label;
		public $defaultValue;
		public $validation;
		public function __construct($label, $defaultValue, $validation=null) {
			$this->label = $label;
			$this->defaultValue = $defaultValue;
			$this->validation = $validation;
		}
	}
}