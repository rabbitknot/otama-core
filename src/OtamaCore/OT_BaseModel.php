<?php

if (!class_exists('OT_BaseModel', false)) {
	require_once 'OT_BaseRecord.php';
	require_once 'OT_BaseRecordCollection.php';
	require_once 'OT_BaseModelProperty.php';

	class OT_BaseModel
	{
		protected static $_singleton;

		public static function singleton()
		{
			if (empty(self::$_singleton)) {
				$class = get_called_class();
				self::$_singleton = new $class();
			}
			return self::$_singleton;
		}

		protected function db() {
			global $wpdb;
			return $wpdb;
		}

		protected $tableName = '';

		protected function table() {
			return $this->db()->prefix.$this->tableName;
		}

		/**
		 * モデルが管理する属性の一覧
		 * @var array カラム名 => array(表示名, デフォルト値, バリデーションルール)
		 */
		protected $attrs = array();

		/**
		 * このモデルが利用するレコード名。
		 * 独自の Record オブジェクトを利用したい場合は指定する。
		 * デフォルトは BaseRecord が利用される。
		 */
		protected $RecordClassName  = null;

		/**
		 * このモデルが利用するレコードコレクション名。
		 * 独自の Record オブジェクトを利用したい場合は指定する。
		 * デフォルトは OTBaseRecordCollection が利用される。
		 */
		protected $RecordCollectionClassName  = null;


		public function __construct() {

			foreach ($this->attrs as $name => &$prop) {
				$prop = call_user_func_array('OTBaseModelProperty::create', $prop);
			}

			//Record/RecordCollection クラスの自動検索
			$basename = get_class($this);
			$basename = preg_replace('/Model$/', '', $basename);
			if (empty($this->RecordClassName)) {
				if (class_exists($name = $basename . '_Record')) {
					$this->RecordClassName = $name;
				} else if (class_exists($name = $basename . 'Record')) {
					$this->RecordClassName = $name;
				} else {
					$this->RecordClassName = 'BaseRecord';
				}
			}
			if (empty($this->RecordCollectionClassName)) {
				if (class_exists($name = $basename . '_RecordCollection')) {
					$this->RecordCollectionClassName = $name;
				} else if (class_exists($name= $basename . 'RecordCollection')) {
					$this->RecordCollectionClassName = $name;
				} else {
					$this->RecordCollectionClassName = 'OTBaseRecordCollection';
				}
			}
		}

		/**
		 * 属性定義に指定したバリデーションを適用する。
		 * @param BaseRecord $record
		 * @return void
		 */
		public function validate_record_by_definition($record) {
			foreach ($this->attrs as $name => $definition) {
				$record->validate_for($name, $definition->validation);
			}
		}

		/**
		 * 新しいレコードを作成する
		 * @param array $attrs 新しいレコードにセットする属性値
		 * @return BaseRecord
		 */
		public function build_record($attrs=array()) {
			if (is_object($attrs)) {
				$attrs = get_object_vars($attrs);
			}
			//デフォルト値を設定
			foreach ($this->attrs as $name => $prop) {
				if (!isset($attrs[$name])) {
					$attrs[$name] = $prop->defaultValue;
				}
			}
			$record = new $this->RecordClassName($this, $attrs);
			return $record;
		}

		/**
		 * レコードの一覧を作成する。
		 * @param array<array> $rows
		 * @param callable $builder レコードを生成するためのビルダー
		 * @return BaseRecord
		 */
		public function build_record_collection($rows=array(), $builder=null) {
			return new $this->RecordCollectionClassName($this, $rows, $builder);
		}

		/**
		 * $attrs からモデルの永続化対象の属性のみを取得する。
		 * @param array $attrs
		 * @return array
		 */
		public function extract_persistent_data($attrs) {
			return array_intersect_key($attrs, $this->attrs);
		}

		public function get_attr_name($key) {
			$label = '';
			isset($this->attrs[$key])
			and $label = $this->attrs[$key]->label;
			return $label;
		}

	}
}