<?php

if (!class_exists('OT_Validator', false)) {

}
class OT_Validator {
    public function checkInputData($inputData) {
        // エラーメッセージを格納する配列
        $err_msg = array();

        // 各入力内容を取得
        $name = isset($inputData["name"]) ? $inputData["name"] : "";
        $email = isset($inputData["email"]) ? $inputData["email"] : "";
        $reply_method = isset($inputData["reply_method"]) ? $inputData["reply_method"] : "";
        $message = isset($inputData["message"]) ? $inputData["message"] : "";

        // お名前
        if (strlen($name) == 0) {
            $err_msg[] = "お名前を入力してください。";
        }
        // メールアドレス
        if (strlen($email) == 0) {
            $err_msg[] = "メールアドレスを入力してください。";
        } else {
            if (!preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $email)) {
                $err_msg[] = "正しいメールアドレスを入力してください。";
            }
        }
        // ご連絡方法
        if (strlen($reply_method) == 0) {
            $err_msg[] = "ご連絡方法を選択して下さい。";
        }
        // お問い合わせ内容
        if (strlen($message) == 0) {
            $err_msg[] = "お問い合わせ内容を入力してください。";
        }

        return $err_msg;
    }

	public static function valid($value, $validation_pattern, &$err_msg) {
		$_value = $value;

		if (is_array($validation_pattern)) {
			foreach ($validation_pattern as $vp) {
				try {
					$_value = OT_Validator::$vp($_value);
				} catch (OT_ValidationError $ve) {
					$err_msg[] = $ve->getMessage();
				}
			}
		} else {
			try {
				$_value = OT_Validator::$validation_pattern($value);
			} catch (OT_ValidationError $ve) {
				$err_msg[] = $ve->getMessage();
			}
		}
		return $_value;
	}

    public function replyMethodtoJapanese($replyMethod) {
        switch ($replyMethod) {
            case 'either':
                return 'メール，お電話どちらでも良い';
                break;
            case 'email':
                return 'メール';
            case 'tel':
                return 'お電話';
            default:
                return '選択されていません';
        }
    }

    public function isCheckedHowToContact($value, $method) {
        if (!isset($value) && $method === 'either') {
            return 'checked';
        }
        return ($value == $method) ? 'checked' : '';
    }

    // ↓↓↓　from Foreignkey validator

	public static function apply($value, $rules, $onerr = null, $attr = null)
	{
		if (empty($rules)) {
			return $value;
		}
		if (!is_array($rules)) {
			$rules = explode('|', $rules);
		}
		foreach ($rules as $rule) {
			try {
				$_ = explode('(', $rule, 2);
				if (1 == sizeof($_)) {
					$rule = $_[0];
					$args = array();
				} else {
					$rule = $_[0];
					$args = trim($_[1], ')');
					$args = preg_split('/[,\s]+/', $args);
				}
				if (method_exists( 'OT_Validator', $rule)) {
					$validator = "validator::$rule";
				} else if (function_exists($rule)) {
					$validator = $rule;
				}
				if (empty($validator)) {
					throw new ErrorException(
						"Validater not found. Given name rule is " . $rule);
				}
				array_unshift($args, $value);
				$value = call_user_func_array($validator, $args);
			} catch (OT_ValidationError $e) {
				if (is_callable($onerr)) {
					$errmsg = $e->getMessage();
					$onerr($errmsg, $rule);
				} elseif ($onerr instanceof DNV_Record) {
					$onerr->adderr($attr, $e->getMessage());
				}
			}
		}
		return $value;
	}


	/**
	 * 人物のログインID用
	 */
	public static function user_login_id($value)
	{
		$value = self::trim($value);
		if ($value === '' or is_null($value)) return $value;
		$value = self::an1($value);
		$chars = preg_quote('!"#$%\'()-^¥=~|@[`{;:]+*}./<>?', '/');
		if (!preg_match("/^[0-9a-zA-Z$chars]+$/", $value))
			throw new OT_ValidationError('英数字で入力してください。');
		return $value;
	}

	/**
	 * 人物のログインパスワード用
	 */
	public static function user_login_pwd($value)
	{
		$value = self::trim($value);
		if ($value === '' or is_null($value)) return $value;
		$value = self::an1($value);
		$chars = preg_quote('!"#$%\'()-^¥=~|@[`{;:]+*}./<>?', '/');
		if (!preg_match("/^[0-9a-zA-Z$chars]+$/", $value))
			throw new OT_ValidationError('英数字で入力してください。');
		return $value;
	}

	/**
	 * 固有名称
	 */
	public static function name_text($value)
	{
		$value = self::trim($value);
		return $value;
	}

	/**
	 * 固有名称のふりがな
	 */
	public static function kana_text($value)
	{
		$value = self::trim($value);
		$value = self::hira2all($value);
		if ($value === '' or is_null($value)) return $value;
		if (!preg_match('/^[ぁ-ゞー 　]+$/u', $value))
			throw new OT_ValidationError('ひらがなで入力してください。');
		return $value;
	}

	/**
	 * ローマ字入力
	 */
	public static function romaji_text($value)
	{
		$value = self::trim($value);
		if ($value === '' or is_null($value)) return $value;
		$value = self::an1($value);
		if (!preg_match('/^[\d\w ]*$/', $value))
			throw new OT_ValidationError('英字で入力してください。');
		return $value;
	}

	/**
	 * 住所テキスト
	 */
	public static function address_text($value)
	{
		$value = self::trim($value);
		return $value;
	}

	/**
	 * その他の一般的なテキスト
	 */
	public static function stndard_text($value)
	{
		$value = self::trim($value);
		return $value;
	}


	public static $an_hankaku = array('-');
	public static $an_zenkaku = array('ー',);

	/**
	 * 文字列の前後の空白文字を除去する。
	 * @param string $value
	 * @param string $charlist 空白として除去する文字を指定。デフォルト：" \t\n\r\0\x0B"
	 * @return string
	 */
	public static function trim($value, $charlist = " \t\n\r\0\x0B")
	{
		if ($value === '' or is_null($value)) return $value;
		return trim($value, $charlist);
	}


	/**
	 * 英数、カナ、スペースの全角をすべて半角に
	 * ひらがながあれば半角カナに
	 * @param string $value
	 * @return string
	 */
	public static function zn2hn($value)
	{
		if ($value === '' or is_null($value)) return $value;
		return mb_convert_kana($value, 'askhV');
	}

	public static function bundle_spaces2one($value)
	{
		if ($value === '' or is_null($value)) return $value;
		$value = preg_replace('/\s(?=\s)/', '', $value);
		return $value;
	}

	/**
	 * テキストを英数字を半角文字に、カタカナを全角文字に正規化する。
	 * @param string $value
	 * @return string
	 */
	public static function normtext($value)
	{
		if ($value === '' or is_null($value)) return $value;
		$value = self::trim($value);
		$value = mb_convert_kana($value, 'aKV');
		return $value;
	}

	/**
	 * アルファベットを小文字に変換する
	 * @param string $value
	 * @return string
	 */
	public static function lowercase($value)
	{
		if ($value === '' or is_null($value)) return $value;
		return mb_convert_case($value, MB_CASE_LOWER);
	}

	/**
	 * アルファベットを大文字に変換する
	 * @param string $value
	 * @return string
	 */
	public static function uppercase($value)
	{
		if ($value === '' or is_null($value)) return $value;
		return mb_convert_case($value, MB_CASE_UPPER);
	}

	/**
	 * alphanum_to_single のエイリアス
	 * @param string $value
	 * @return string
	 */
	public static function an1($value)
	{
		return self::alphanum_to_single($value);
	}

	/**
	 * 英数字を半角文字に統一する
	 * @param string $value
	 * @return string
	 */
	public static function alphanum_to_single($value)
	{
		if ($value === '' or is_null($value)) return $value;
		$value = str_replace(self::$an_zenkaku, self::$an_hankaku, $value);
		return mb_convert_kana($value, 'a');
	}

	/**
	 * alphanum_to_double のエイリアス
	 * @param string $value
	 * @return string
	 */
	public static function an2($value)
	{
		return self::alphanum_to_double($value);
	}

	/**
	 * 英数字を全角文字に統一する
	 * @param string $value
	 * @return string
	 */
	public static function alphanum_to_double($value)
	{
		if ($value === '' or is_null($value)) return $value;
		$value = str_replace(self::$an_hankaku, self::$an_zenkaku, $value);
		return mb_convert_kana($value, 'A');
	}

	/**
	 * kana_to_double のエイリアス
	 * @param string $value
	 * @return string
	 */
	public static function kana2($value)
	{
		return self::kata_to_double($value);
	}

	/**
	 * カタカナを２バイト文字に統一する
	 * @param string $value
	 * @return string
	 */
	public static function kata_to_double($value)
	{
		if ($value === '' or is_null($value)) return $value;
		return mb_convert_kana($value, 'KV');
	}

	/**
	 * kana_to_double_all のエイリアス
	 * @param string $value
	 * @return string
	 */
	public static function kata2all($value)
	{
		return self::kata_to_double_all($value);
	}

	/**
	 * ひらがな、カタカナを全て２バイトのカタカナに統一する
	 * @param string $value
	 * @return string
	 */
	public static function kata_to_double_all($value)
	{
		if ($value === '' or is_null($value)) return $value;
		return mb_convert_kana($value, 'KVC');
	}

	/**
	 * hira_to_double_all  のエイリアス
	 * @param string $value
	 * @return string
	 */
	public static function hira2all($value)
	{
		return self::hira_to_double_all($value);
	}

	/**
	 * ひらがな、カタカナを全て２バイトのひらがなに統一する
	 * @param string $value
	 * @return string
	 */
	public static function hira_to_double_all($value)
	{
		if ($value === '' or is_null($value)) return $value;
		return mb_convert_kana($value, 'HVc');
	}


	/**
	 * 入力された年月をdate形式に整える
	 */
	public static function year_and_month($value)
	{
		if (empty($value)) {
			return null;
		}
		$value = mb_convert_kana($value, 'a');
		$value = trim(str_replace('/\D/', '-', $value), '-');
		$matches = array();
		preg_match('/^[0-9]{4,4}/', $value, $matches);
		$year = $matches[0];
		preg_match('/[0-9]{1,2}$/', $value, $matches);
		$month = $matches[0];
		if (1 == mb_strlen($month)) {
			$month = '0' . $month;
		}
		return $year . '-' . $month;
	}


	//--------


	/**
	 * 内容があるか確認する
	 * @param string $value
	 * @return string
	 */
	public static function required($value)
	{
		$value = self::trim($value);
		if ($value === '' or is_null($value)) {
			throw new OT_ValidationError('入力が必要です。');
		}
		return $value;
	}

	/**
	 * Eメールか確認する
	 * @param string $value
	 * @return string
	 */
	public static function email($value)
	{
		if ($value === '' or is_null($value)) return $value;
		$value = self::trim($value);
		$value = self::alphanum_to_single($value);
		if (!preg_match('/^(?:(?:(?:(?:[a-zA-Z0-9_!#\$\%&\'*+\/=?\^`{}~|\-]+)(?:\.(?:[a-zA-Z0-9_!#\$\%&\'*+\/=?\^`{}~|\-]+))*)|(?:"(?:\\[^\r\n]|[^\\"])*")))'
			. '\@(?:(?:(?:(?:[a-zA-Z0-9_!#\$\%&\'*+\/=?\^`{}~|\-]+)(?:\.(?:[a-zA-Z0-9_!#\$\%&\'*+\/=?\^`{}~|\-]+))*)|(?:\[(?:\\\S|[\x21-\x5a\x5e-\x7e])*\])))$/', $value))
			throw new OT_ValidationError('メールアドレスではありません。');
		return $value;
	}

	/**
	 * 郵便番号か確認する
	 * @param string $value
	 * @return string
	 */
	public static function zipcode($value)
	{
		if ($value === '' or is_null($value)) return $value;
		$value = self::trim($value);
		$value = self::an1($value);
		if (!preg_match('/^\d{3}-\d{4}$/', $value))
			throw new OT_ValidationError('郵便番号ではありません（例：123-4567）。');
		return $value;
	}

	/**
	 * 郵便番号か確認する
	 * @param string $value
	 * @return string
	 */
	public static function zipcode_cj($value)
	{
		if ($value === '' or is_null($value)) return $value;
		$value = self::trim($value);
		$value = self::an1($value);
		$value = preg_replace('/[^\d]+/', '', $value);
		if (!preg_match('/^\d{7,10}$/', $value))
			throw new OT_ValidationError('7〜10桁の数字で入力ください（例：1234567）。');
		return $value;
	}

	/**
	 * アルファベットのみ（\w*）か確認する。
	 * @param string $value
	 * @return string
	 */
	static function alpha($value)
	{
		if ($value === '' or is_null($value)) return $value;
		$value = self::an1($value);
		if (!preg_match('/^[\w]*$/', $value))
			throw new OT_ValidationError('アルファベットで入力してください。');
		return $value;
	}

	/**
	 * 英数字のみ（[\d\w]*）か確認する。
	 * @param string $value
	 * @return string
	 */
	static function alphanum($value)
	{
		if ($value === '' or is_null($value)) return $value;
		$value = self::an1($value);
		if (!preg_match('/^[\d\w]*$/', $value))
			throw new OT_ValidationError('英数字で入力してください。');
		return $value;
	}

	/**
	 * 数字のみ（[\d]*）か確認する。
	 * @param string $value
	 * @return string
	 */
	static function numelic($value)
	{
		if ($value === '' or is_null($value)) return $value;
		$value = self::an1($value);
		$value = str_replace(',', '', $value);
		if (!preg_match('/^[\d]*$/', $value))
			throw new OT_ValidationError('数字で入力してください。');
		return $value;
	}

	/**
	 * 整数（-?[0-9]+）か確認する。
	 * @param string $value
	 * @return string
	 */
	static function integer($value)
	{
		if ($value === '' or is_null($value)) return $value;
		$value = self::an1($value);
		$value = str_replace(',', '', $value);
		$value = preg_replace('/[^\-\d\.]/', '', $value); //数を構成する文字以外を削除
		if (!preg_match('/^-?[0-9]+$/', $value))
			throw new OT_ValidationError('整数で入力してください。');
		return $value;
	}

	/**
	 * 実数（-?[0-9]+(\.[0-9]+)?）か確認する。
	 * @param string $value
	 * @return string
	 */
	static function real($value)
	{
		if ($value === '' or is_null($value)) return $value;
		$value = self::an1($value);
		$value = str_replace(',', '', $value);
		$value = preg_replace('/[^\-\d\.]/', '', $value); //数を構成する文字以外を削除
		if (!preg_match('/^-?[0-9]+(\.[0-9]+)?$/', $value))
			throw new OT_ValidationError('整数または少数で入力してください。');
		return $value;
	}

	/**
	 * URL か確認する。
	 * @param string $value
	 * @return string
	 */
	static function url($value)
	{
		if ($value === '' or is_null($value)) return $value;
		$value = self::an1($value);
		if (!preg_match('/^https?:\/\//', $value))
			$value = 'http://' . $value;
		if (!preg_match('/^https?(:\/\/[-_.!~*\'()a-zA-Z0-9;\/?:\@&=+\$,%#]+)$/', $value))
			throw new OT_ValidationError('URLではありません。');
		return $value;
	}

	/**
	 * phone のエイリアス
	 */
	static function tel($value)
	{
		return self::phone($value);
	}

	/**
	 * 電話番号か確認する。
	 * @param string $value
	 * @return string
	 */
	static function phone($value)
	{
		if ($value === '' or is_null($value)) return $value;
		$value = self::trim($value);
		$value = self::an1($value);
		if (!preg_match('/^\+?[0-9]+\-[\-\(\)0-9]+$/', $value))
			throw new OT_ValidationError('電話番号ではありません。');
		return $value;
	}

	/**
	 * 日付かどうか確認する
	 * @param $value
	 * @param $format 日付と確認された場合、値を標準化するフォーマット。デフォルトは、'Y-m-d'
	 * @return boolean
	 */
	static function date($value, $format = 'Y-m-d')
	{
		if (empty($value)) {
			$value = '';
		}
		if ($value === '' or is_null($value)) return $value;
		$value = self::an1($value);
		$time = strtotime($value);
		if ($time === false) {
			throw new OT_ValidationError('日付として評価できません。');
		} else {
			return date($format, $time);
		}

		// $datetime = new Datetime2($value);
		// if (! $datetime->timestamp)
		// 	throw new ValidationError('日付として評価できません。');
		// return $datetime->to_string($format);
	}

	/**
	 * 日付と時間かどうか確認する
	 * @param $value
	 * @param $format 日付と確認された場合、値を標準化するフォーマット。デフォルトは、'Y-m-d H:i:s'
	 * @return boolean
	 */
	static function datetime($value, $format = 'Y-m-d H:i:s')
	{
		if (empty($value)) {
			$value = '';
		}
		if ($value === '' or is_null($value)) return $value;
		$value = self::an1($value);
		$time = strtotime($value);
		if ($time === false) {
			throw new OT_ValidationError('日時として評価できません。');
		} else {
			return date($format, $time);
		}
		// $datetime = new Datetime2($value);
		// if (! $datetime->timestamp)
		// 	throw new ValidationError('日時として評価できません。');
		// return $datetime->to_string($format);
	}

	/**
	 * パスワードか否か確認する
	 * @param $value
	 * @param $chars パスワードに使える文字列の正規表現。デフォルトは、'Y\d\w'
	 * @return boolean
	 */
	static function password($value, $chars = '\d\w')
	{
		if ($value === '' or is_null($value)) return $value;
		if (!preg_match('/^[' . $chars . ']+$/', $value))
			throw new OT_ValidationError('パスワードは英数字で入力してください。');
		return $value;
	}

	/**
	 * 最大値を確認する。
	 * $valueが文字列なら文字数で評価し、数値なら値の大きさで評価します。
	 * @param $value
	 * @param $max 許容する最大値（値を含む）
	 */
	static function max($value, $max)
	{
		if ($value === '' or is_null($value)) return $value;
		if (is_string($value)) {
			if (mb_strlen($value) > $max)
				throw new OT_ValidationError(sprintf('%d文字以内で入力してください (現在%d文字)。', $max, mb_strlen($value)));
		} else if (is_int($value) or is_float($value)) {
			if ($value > $max)
				throw new OT_ValidationError( $max . '以下の値を入力してください。');
		}
		return $value;
	}

	/**
	 * 最小値を確認する。
	 * $valueが文字列なら文字数で評価し、数値なら値の大きさで評価します。
	 * @return string | null
	 * @param $value
	 * @param $min 許容する最小値（値を含む）
	 */
	static function min($value, $min)
	{
		if ($value === '' or is_null($value)) return $value;
		if (is_string($value)) {
			if (mb_strlen($value) < $min)
				throw new OT_ValidationError(sprintf('%d文字以上で入力してください (現在%d文字)。', $min, mb_strlen($value)));
		} else if (is_int($value) or is_float($value)) {
			if ($value < $min)
				throw new OT_ValidationError( $min . '以上の値を入力してください。');
		}
		return $value;
	}

	/**
	 * 入力を各種アルゴリズムでハッシュ化する。
	 * 関数 hash のラップ
	 * @return string | null
	 * @param $str
	 * @param $algo
	 */
	static function hash($value, $algo = 'md5')
	{
		if ($value === '' or is_null($value)) return $value;
		return hash($algo, $value);
	}

	/**
	 * 拡張子を標準化する。例 jpeg => jpg
	 * @return string | null
	 * @param unknown_type $ext
	 */
	static function regular_extension($value)
	{
		static $map = array('jpg' => 'jpeg');
		if ($value === '' or is_null($value)) return $value;
		$value = strtolower($value);
		return isset($map[$value]) ? $map[$value] : $value;
	}

	/**
	 * RGB16進数カラーコードであることを確認する。
	 * @return string | null
	 * @param unknown_type $ext
	 */
	static function rgb16code($value)
	{
		if ($value === '' or is_null($value)) return $value;
		$value = self::trim($value);
		$value = self::an1($value);
		$value = self::trim($value, '#');
		$value = strtoupper($value);

		if (!preg_match('/^(?:[0-9A-F]{3}|[0-9A-F]{6})$/', $value)) {
			throw new OT_ValidationError('16進数のRGBカラーコードで入力してください。');
		}

		$value = preg_replace('/^([0-9A-F])([0-9A-F])([0-9A-F])$/', '$1$1$2$2$3$3', $value);
		return $value;
	}

	/**
	 * 文字列から HTML および PHP タグを取り除く
	 * @return string | null
	 * @param string $text
	 */
	static function striptags($text)
	{
		if ($text === '' or is_null($text)) return $text;
		return strip_tags($text);
	}
}