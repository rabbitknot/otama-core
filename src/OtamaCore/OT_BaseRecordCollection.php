<?php

if (!class_exists('OT_BaseRecordCollection', false)) {
	class OT_BaseRecordCollection implements \Iterator, \ArrayAccess, \Countable, \Serializable {

		private $_model;
		private $_records;
		private $_index;
		//private $_builder;



		public function __construct($model, $records) {

			$this->_model = $model;
			$this->_records = $records;

			// if (empty($builder)) {
			// 	$builder = function($data, $key, $model) {
			// 		$record = $model->build_record($data);
			// 		$record->is_new(false);
			// 		return $record;
			// 	};
			// }

			// $this->_builder = $builder;
		}

		private function builder($data, $key, $model) {
			$record = $model->build_record($data);
			$record->is_new(false);
			return $record;
		}

		public function _get($index) {
			$record = $this->_records[$index];
			if (!($record instanceof BaseRecord)) {

				//$builder = $this->_builder;
				$record = $this->builder(
					$record,
					$index,
					$this->_model);

				$this->_records[$index] = $record;
			}
			return $record;
		}

		//Iterator
		public function current() {
			return $this->valid() ? $this->_get($this->_index) : null;
		}

		public function key() {
			return $this->_index;
		}

		public function next() {
			$this->_index++;
		}

		public function prev() {
			$this->_index--;
		}

		public function rewind($pos=0) {
			$this->_index = $pos;
		}

		public function valid() {
			return isset($this->_records[$this->_index]);
		}

		public function has_next() {
			return isset($this->_records[$this->_index + 1]);
		}

		//ArrayAccess
		public function offsetExists($index) {
			return isset($this->_records[$index]);
		}

		public function offsetGet($index) {
			return $this->_get($index);
		}

		/**
		 * @param mixed $index
		 * @param mixed $value
		 *
		 * @throws \ErrorException
		 */
		public function offsetSet($index, $value) {
			throw new \ErrorException('Not supported.', 500);
		}

		public function offsetUnset($index) {
			unset($this->_records[$index]);
		}


		//Countable
		public function count() {
			return count($this->_records);
		}


		//Serializable
		public function serialize() {
			return serialize($this->_records);
		}

		public function getRecords() {
			return $this->_records;
		}

		/**
		 * @param string $serialized
		 *
		 * @throws \ErrorException
		 */
		public function unserialize($serialized) {
			throw new \ErrorException('Not supported');
		}

	}
}