<?php

if (!class_exists('OT_Render', false)) {
	/**
	 * PHP 自体をテンプレートエンジンとして利用するための簡単なユーティリティ
	 */
	class OT_Render
	{
		public static function getInstance() {
			return self::$instance;
		}

		private static $instance;

		public static $_default_view_directory = '';
		public static $_default_view_file_sufix = '.php';

		public $view_directory = '';
		public $view_file_sufix = '';

		private $vars = array();

		private $extend_view = null;
		private $extend_vars = array();
		private $extend_ancestor_blocks = array();

		private $block_stack = array();
		private $block_contents = array();

		private $capture_stack = array();

		public static function setDefault($view_directory, $view_file_sufix = null) {
			self::$_default_view_directory = $view_directory;
			if (!empty($view_file_sufix)) self::$_default_view_file_sufix = $view_file_sufix;
		}

		public function __construct($view_directory = null, $view_file_sufix = null)
		{
			$this->view_directory = empty($view_directory) ? self::$_default_view_directory : $view_directory ;
			$this->view_file_sufix = empty($view_file_sufix) ? self::$_default_view_file_sufix : $view_file_sufix;
			self::$instance = $this;
		}

		/**
		 * テンプレート変数をクリアする
		 */
		public function clear_vars() {
			$this->vars = array();
			$this->extend_view = null;
			$this->extend_vars = array();
			$this->extend_ancestor_blocks = array();
			$this->block_stack = array();
			$this->block_contents = array();
			$this->capture_stack = array();
		}

		/**
		 * セットされている変数を JSON として出力する。
		 * @param string|array<string> $includes  出力したい変数名を指定する。未設定時は全て。
		 * @return void
		 */
		public function vars_to_json($includes=null) {
			$vars = $this->vars;
			if (0 < func_num_args()) {
				$includes = func_get_args();
				$includes = array_flip($includes);
				$vars = array_intersect_key($vars, $includes);
			}
			echo json_encode($vars);
		}

		/**
		 * ビューをレンダリングしてデフォルト変数に保存する。
		 * @param string $view  ビューファイル
		 * @param string $varname  変数名
		 * @param array $vars  このレンダリングで使用する変数
		 * @return void
		 */
		public function to_setvar($view, $varname, $vars=array()) {
			$renderd = $this->to_string($view, $vars);
			$this->setvar($varname, $renderd);
		}

		/**
		 * ビューをレンダリングして結果を文字列で取得する。
		 * @param string $view  ビューファイル
		 * @param array $vars  このレンダリングで使用する変数
		 * @return string
		 */
		public function to_string($view, $vars=array()) {
			ob_start();
			$this->display($view, $vars);
			return ob_get_clean();
		}

		/**
		 * $view をレンダリングして表示する。
		 * @param string $view  ビューファイル
		 * @param array $vars  このレンダリングで使用する変数
		 * @return void
		 */
		public function display($view, $vars=array()) {
			$____view = $this->get_view_filepath($view);
			$____vars = $vars;
			extract($this->vars); //デフォルト指定の変数
			extract($this->extend_vars); //継承による変数
			extract($____vars); //ここで指定された変数
			require $____view;
		}

		/**
		 * ビューファイルのパスを取得する。
		 * @param string $view  ビューファイル
		 * @return string
		 */
		private function get_view_filepath($view) {
			return $this->view_directory
				. DIRECTORY_SEPARATOR
				. $view . $this->view_file_sufix;
		}

		/**
		 * レンダラのデフォルト変数を設定する。
		 * レンダラのデフォルト変数は、全てのレンダリングで利用できる。
		 * 連想配列、または名前を値を指定して設定できる。
		 * @param mixed $one
		 * @param mixed $two
		 * @return void
		 */
		public function setvar($one, $two=null) {
			if (is_array($one)) {
				$this->vars = array_merge($this->vars, $one);
			} else {
				$this->vars[$one] = $two;
			}
		}

		/**
		 * レンダラのデフォルト変数を削除する。
		 * @param mixed $name
		 * @return void
		 */
		public function clearvar($name) {
			unset($this->vars[$name]);
		}

		/**
		 * デフォルト変数をクリアする。
		 * @return void
		 */
		public function clearvars() {
			$this->vars = array();
		}

		/**
		 * レンダラのデフォルト変数を取得する。
		 * @param string $name  変数名
		 * @param mixed $default  変数が未定義の時に返される値
		 * @return mixed
		 */
		public function getvar($name, $default=null) {
			return isset($this->vars[$name]) ? $this->vars[$name] : $default;
		}

		/**
		 * 継承を開始する。
		 * @param string $view  ビューファイル
		 * @param array $vars  継承するテンプレートで利用したい変数
		 * @return void
		 */
		public function extend($view, $vars=array()) {
			$this->extend_view = $view;
			$this->extend_vars = array_merge($vars, $this->extend_vars); //開発注：この順番は正しい。$this->>extend_vars の方が、より子で指定された値になる
			$this->capture();
		}

		/**
		 * 継承を終了する
		 * @return void
		 */
		public function end_extend() {
			$content = $this->end_capture();
			$view = $this->extend_view;
			$vars = $this->extend_vars;
			$this->extend_view = null;
			$this->block_contents['__content'] = array($content, 'append');
			$this->display($view, $vars);
		}

		/**
		 * 継承の祖先にブロックがあるか確認する
		 * @param string $block
		 * @return bool
		 */
		private function exists_block_in_ancestors($block) {
			if (empty($this->extend_view)) {
				return array();
			} else {
				$ancestor_blocks = $this->get_ancestor_blocks($this->extend_view);
				return in_array($block, $ancestor_blocks);
			}
		}

		/**
		 * 継承の祖先に含まれるブロック名の一覧を返す。
		 * @param string $parent  継承したビューファイル
		 * @return array<string>
		 */
		private function get_ancestor_blocks($parent) {
			$parent = $this->get_view_filepath($parent);
			if (!isset($this->extend_ancestor_blocks[$parent])) {
				$blocks = array();
				$view = file_get_contents($parent);
				if (preg_match_all('/render::(?:block|load_block)\(\s*["\']([^"\']+)["\']\s*/', $view, $matches, PREG_PATTERN_ORDER)) {
					$blocks = $matches[1];
				}
				if (preg_match('/render::extend\(\s*["\']([^"\']+)["\']\s*/', $view, $matches)) {
					$more_parent = $matches[1];
					$ancestor_blocks = $this->get_ancestor_blocks($more_parent);
					$blocks = array_merge($blocks, $ancestor_blocks);
					$blocks = array_unique($blocks);
				}
				$this->extend_ancestor_blocks[$parent] = $blocks;
			}
			return $this->extend_ancestor_blocks[$parent];
		}

		/**
		 * ブロックの定義を開始する
		 * @param string $block  ブロックの名前
		 * @param mixed $method  既にブロック変数が定義済みの場合、このブロックの内容を後ろに追加する場合には 'append' を指定、前に追加する場合には 'prepend' を指定する。デフォルトは false で追加せず上書きする
		 * @return void
		 */
		public function block($block, $method=false) {
			array_push($this->block_stack, array($block, $method));
			$this->capture();
		}

		/**
		 * ブロックの定義を開始する。
		 * ブロックは継承元ブロックの後ろに追加される。
		 * @param string $block  ブロックの名前
		 * @return void
		 */
		public function block_append($block) {
			array_push($this->block_stack, array($block, 'append'));
			$this->capture();
		}

		/**
		 * ブロックの定義を開始する。
		 * ブロックは継承元ブロックの前に追加される。
		 * @param string $block  ブロックの名前
		 * @return void
		 */
		public function block_prepend($block) {
			array_push($this->block_stack, array($block, 'prepend'));
			$this->capture();
		}

		/**
		 * ブロックの定義を終了する
		 * @return string ブロックの内容
		 */
		public function end_block() {
			$content = $this->end_capture();
			list($block, $method) = array_pop($this->block_stack);

			//オーバーライドされたブロックがある場合（既にブロックが存在する場合）
			if (isset($this->block_contents[$block])) {
				list($pre_content, $pre_method) = $this->block_contents[$block];
				//前に追加
				if ('append' === $pre_method) {
					$content = $content . $pre_content;
				}
				//前に追加
				else if ('prepend' === $pre_method) {
					$content = $pre_content . $content;
				}
				//オーバーライド
				else {
					$content = $pre_content;
				}
			}

			//ブロックデータとして保存
			$this->block_contents[$block] = array($content, $method);

			//上位にブロックが無いとき、または __content ブロックなら出力
			if ($block === '__content' or !$this->exists_block_in_ancestors($block)) {
				echo $content;
			}

			return $content;
		}

		/**
		 * 登録されているブロックをロードする
		 * @param string $block ブロックの名前
		 * @return void
		 */
		public function load_block($block) {
			$this->block($block);
			$this->end_block();
		}

		/**
		 * extend でオーバーライドされたコンテンツをロードする
		 * @return void
		 */
		public function load_content() {
			$this->block('__content');
			$this->end_block();
		}

		/**
		 * キャプチャーを開始する
		 * @param $var_name キャプチャした内容を保存する変数名
		 * @param $override[optinal:false] キャプチャを変数に保存するとき、既存の変数を上書きするか追記するか。
		 * @return void
		 */
		public function capture($var_name=null, $override=false) {
			array_push($this->capture_stack, array($var_name, $override));
			ob_start();
		}

		/**
		 * キャプチャーを終了する
		 * @return string キャプチャした内容
		 */
		public function end_capture() {
			$captured = ob_get_clean();
			list($var_name, $override) = array_pop($this->capture_stack);
			if (!empty($var_name)) {
				if (!$override) {
					$captured = $this->getvar($var_name) . $captured;
				}
				$this->setvar($var_name, $captured);
			}
			return $captured;
		}

		/**
		 * キャプチャーを終了する
		 * もしキャプチャした内容が空なら、$alt を評価して返す。
		 * @return string キャプチャした内容
		 */
		public function end_capture_or($alt) {
			$captured = $this->end_capture();

			if (trim($captured)) {
				return $captured;
			} else if (is_callable($alt)) {
				$alt();
			} else {
				return $alt;
			}
		}


		/**
		 * 改行やタブを取り除く範囲を開始する
		 * @return void
		 */
		public function strip() {
			$this->capture();
		}

		/**
		 * 改行やタブを取り除く範囲を終了する
		 * @return void
		 */
		public function end_strip() {
			$content = $this->end_capture();
			$content = str_replace("\n", '', $content);
			$content = str_replace("\r", '', $content);
			$content = str_replace("\t", '', $content);
			echo $content;
		}

	}
}