<?php

if (!class_exists('OT_Params', false)) {
	class OT_Params
	{

		static function get($name, $default=null) {
			if (isset($_GET[$name])) {
				return $_GET[$name];
			} else if (!is_string($default) and is_callable($default)) {
				return $default($name);
			} else {
				return $default;
			}
		}

		static function post($name, $default=null) {
			if (isset($_POST[$name])) {
				return $_POST[$name];
			} else if (!is_string($default) and is_callable($default)) {
				return $default($name);
			} else {
				return $default;
			}
		}

	}
}