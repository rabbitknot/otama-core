<?php

/**
 * レコードのエラーを管理する。
 */

if (!class_exists('OT_RecordErrorCollection', false)) {
	class OT_RecordErrorCollection
	{
		private $_errors;

		public function __construct() {
			$this->_errors = array();
		}

		/**
		 * エラーメッセージを登録する
		 * @param string $name
		 * @param string $message
		 * @return bool
		 */
		public function add($name, $message) {
			$this->_errors[$name][] = $message;
		}

		/**
		 * エラーメッセージを取得する。
		 * @param string $name
		 * @return bool
		 */
		public function get($name, $glue='') {
			$message = null;
			if (isset($this->_errors[$name])) {
				$message =  implode($glue, $this->_errors[$name]);
			}
			return $message;
		}

		/**
		 * エラー内容をクリア
		 * @return void
		 */
		public function clear() {
			$this->_errors = array();
		}

		/**
		 * 指定の名前のエラーがあるか？
		 * @param string $name
		 * @return bool
		 */
		public function has($name) {
			return isset($this->_errors[$name]);
		}

		/**
		 * エラーを含んでいるか？
		 * @return bool
		 */
		public function is_empty() {
			return empty($this->_errors);
		}

		public function __toString() {
			$msgs = array();
			foreach ($this->_errors as $key => $err) {
				$msgs[] = sprintf("[%s] %s", $key, $this->get($key));
			}
			return implode("\n", $msgs);
		}
	}
}