<?php

if (!class_exists('OT_AutoLoader', false)) {
	class OT_AutoLoader {
		function __construct()
		{
			$this->_load_directory();
		}

		private function _load_directory()
		{
			foreach (glob(dirname(__FILE__) . '/*.php') as $filename) {
				require_once $filename;
			}
			foreach (glob(dirname(__FILE__) . '/*/*.php') as $filename) {
				require_once $filename;
			}
			foreach (glob(dirname(__FILE__) . '/*/*/*.php') as $filename) {
				require_once $filename;
			}
			return true;
		}
	}
}
