<?php

if (!class_exists('OT_ValidationError', false)) {
	class OT_ValidationError extends \Exception {}
}

if (!class_exists('BaseRecordValidationError', false)) {
	class BaseRecordValidationError extends \ErrorException {}
}

if (!class_exists('OT_RouterError', false)) {
	class OT_RouterError extends \ErrorException {}
}
