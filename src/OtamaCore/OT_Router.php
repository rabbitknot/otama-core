<?php

if (!class_exists('OT_Router', false)) {
	class OT_Router {

		public static function config($_controller, $_method) {
			return array( 'controller' => $_controller, 'method' => $_method);
		}

		public function __construct($pluginPathOrFullPath)
		{
			$this->_set_routing($pluginPathOrFullPath);
		}

		function _set_routing($path) {

			if (is_file($path.'/application/config/routes.php')) {
				include($path.'/application/config/routes.php');
			} else if (is_file($path)) {
				include($path);
			} else {
				throw new OT_RouterError('routesファイルが見つかりません');
			}
			$this->routes = ( ! isset($route) OR ! is_array($route)) ? array() : $route;

			unset($route);
		}

		public function request($_request_param) {

			if (isset($this->routes[$_request_param])) {
				$_controller = $this->routes[$_request_param]['controller'];
				$_method = $this->routes[$_request_param]['method'];

				$controller = OT_Controller::activate_controller($_controller);
				if (method_exists($controller, $_method)) {
					$controller->$_method();
				}
			} else {
				OT_Common::show_404('不正なリクエストです。');
			}

		}
	}
}