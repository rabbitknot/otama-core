<?php

if (!class_exists('OT_Controller', false)) {
	class OT_Controller {

		private static $instance;

		public function __construct()
		{
			self::$instance =& $this;

			//仕様注：プラグイン向けパッケージでは使用しない
			//session_start();
		}

		public static function &get_instance()
		{
			return self::$instance;
		}

		public static function activate_controller($controller_name) {

			if (class_exists($controller_name)) {
				return new $controller_name();
			} else {
				OT_Common::show_404('コントローラーが見つかりません。');
			}
		}
	}
}